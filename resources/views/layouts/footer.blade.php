<footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://greatdayhr.com/id-id/">GreatDay HR</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 1
    </div>
</footer>
